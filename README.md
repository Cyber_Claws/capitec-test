# Capitec Technical Test

---

#### Note: The original task was to calculate Capitec Stock price but due to the unavailability of FREE live JSE stock prices APIs, and CPI not being listed on most free international financial data service providers, I used FaceBook as an example, but this can easily be changed for CPI or CPIP if an API were made available.

This repository is for the Capitech Interview Technical Test: [Live Demo](http://159.89.97.95). This project is a Share calculator Web Component. You provide an amount and it queries the current share price from [QUANDL](https://www.quandl.com/) and calculates how much shares you can afford. The Web Component is in the form on a modal and it will only add a button to the lower right position of your screen. This bottom is fixed for customer convenience and can be accessed at any time during browsing. It features modern design and it is responsive.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Folder Structure

```bash
├── capitec-test
│   ├── capitec-shares-component
|   ├── ├──  capitec-shares.js
│   ├── README.md
│   ├── index.html
```

### Prerequisites

You need an HTTP server for testing and running like [serve](https://www.npmjs.com/package/serve) or [apache2](https://www.apache.org/) and a web browser that supports [Web Components](https://www.webcomponents.org/) without any polyfills, [here](https://www.webcomponents.org/) if a list of supported browsers.


### Installing

You use, you just need to reference the `capitec-shares.js` file in the header on your `HTML5` project then use the tag `<capitec-share-calculator></capitec-share-calculator>` to reference it. The component is fully stand alone and doesn't need any plugins. For an example check `/capitec-test/index.html`.

You also have an option of passing the `<slot>` to costomize the opening button.

```HTML5
    <!--- Custom open botton example--->
    <capitec-share-calculator>
        <span slot="openopenbutton-name">🚪 Open</span>
    </capitec-share-calculator>
```

## Tests

No tests provided in this repo.

## Built With

* [JavaScript](https://www.javascript.com/) - Scripting lanuage
* [CSS](https://www.w3schools.com/css/) - Style Sheet scripting
* [HTML5](https://www.w3schools.com/html/html5_browsers.asp) - Markup language

## Versioning

Uses [SemVer](http://semver.org/) for versioning. Current version `1.0.0`

## Authors

* **Ishmael Sibisi** - *Initial work* - [cyber-claws](https://github.com/cyber-claws)

## License

This project is unlicensed.

## TODO

- [ ] Use JSE API
- [x] Ability to customize open button
- [ ] Write tests