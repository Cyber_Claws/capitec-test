/**
 * HTML imports without libraries deprecated/problematic so had to get creative
 */
const template = document.createElement('template');
template.innerHTML = `
  <style>
    input {
      border: none;
      text-align: center;
    }

    .container {
      display: -webkit-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-align: center;
      -webkit-align-items: center;
      -ms-flex-align: center;
      align-items: center;
      -webkit-box-pack: center;
      -webkit-justify-content: center;
      -ms-flex-pack: center;
      justify-content: center;
      background: rgba(255,255,255,.7);
      position: fixed;
      top: 0;
      left: 0;
      margin: 0;
      height: 100vh;
      width: 100vw;
      z-index: 99;
      transition: all 1.3s cubic-bezier(.25,.8,.25,1);
    }

    input[type=number]::-webkit-inner-spin-button, 
    input[type=number]::-webkit-outer-spin-button { 
      -webkit-appearance: none; 
      margin: 0; 
    }
    
    .button-parent {
      margin-top: 25px;
    }
    button:focus {
      outline: none;
    }
    
    ::-webkit-input-placeholder {
      color: rgba(255, 255, 255, 0.65);
    }
    
    ::-webkit-input-placeholder .input-line:focus +::input-placeholder {
      color: #fff;
    }
    
    .input-line:focus {
      outline: none;
      border-color: #fff;
      -webkit-transition: all .2s ease;
      transition: all .2s ease;
    }
    
    .open-button {
      cursor: pointer;
      background: none;
      border: none;
      border-radius: 0;
      -webkit-align-self: flex-end;
      -ms-flex-item-align: end;
      align-self: flex-end;
      font-size: 19px;
      font-size: 1.2rem;
      line-height: 2.5em;
      margin-top: auto;
      margin-bottom: 25px;
      -webkit-transition: all .2s ease;
      transition: all .2s ease;
      position: fixed;
      right: 20px;
      bottom: 0;
    }
    .main-button {
      cursor: pointer;
      background: none;
      border: 1px solid rgba(255, 255, 255, 0.65);
      border-radius: 0;
      color: rgba(255, 255, 255, 0.65);
      -webkit-align-self: flex-end;
      -ms-flex-item-align: end;
      align-self: flex-end;
      font-size: 19px;
      font-size: 1.2rem;
      font-family: 'Raleway', sans-serif !important;
      font-weight: 300;
      line-height: 2.5em;
      margin-top: auto;
      margin-bottom: 25px;
      -webkit-transition: all .2s ease;
      transition: all .2s ease;
    }
    
    .main-button:hover {
      background: rgba(255, 255, 255, 0.15);
      color: #fff;
      -webkit-transition: all .2s ease;
      transition: all .2s ease;
    }
    
    .input-line {
      background: none;
      margin-bottom: 10px;
      line-height: 2.4em;
      color: #fff;
      font-family: 'Raleway', sans-serif !important;
      font-weight: 300;
      letter-spacing: 0px;
      letter-spacing: 0.02rem;
      font-size: 19px;
      font-size: 1.2rem;
      border-bottom: 1px solid rgba(255, 255, 255, 0.65);
      -webkit-transition: all .2s ease;
      transition: all .2s ease;
    }
    
    .full-width {
      width: 100%;
    }
    
    .input-parent {
      margin-top: 25px;
    }
    
    .content {
      padding-left: 25px;
      padding-right: 25px;
      display: -webkit-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
      -webkit-flex-flow: column;
      -ms-flex-flow: column;
      flex-flow: column;
      z-index: 5;
    }
    
    .title {
      font-weight: 200;
      margin-top: 75px;
      margin-bottom: 20px;
      text-align: center;
      font-size: 2.5rem;
      letter-spacing: 0px;
      letter-spacing: 0.05rem;
    }
    
    .results-show {
      font-weight: 200;
      margin-top: 5px;
      margin-bottom: 2px;
      text-align: center;
      font-size: 1.6rem;
      letter-spacing: 0px;
      display: none;
      letter-spacing: 0.05rem;
    }

    .subtitle {
      text-align: center;
      line-height: 1em;
      font-weight: 100;
      letter-spacing: 0px;
      letter-spacing: 0.02rem;
    }
    .error {
      color: red;
      font-size: 12px;
      background-color: white;
      padding: 5px;
      font-weight: 400;
      display: none;
    }
    .menu {
      background: rgba(0, 0, 0, 0.2);
      width: 100%;
      height: 50px;
    }
    
    .main-calculator {
      z-index: 100;
      color: #fff;
      font-family: 'Raleway', sans-serif !important;
      position: relative;
      display: -webkit-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
      -webkit-flex-flow: column;
      -ms-flex-flow: column;
      flex-flow: column;
      box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
      transition: all 0.5s cubic-bezier(.25,.8,.25,1);
      box-sizing: border-box;
      height: 460px;
      width: 360px;
      background: #fff;
      background: url('https://placeimg.com/640/480/arch') top left no-repeat;
    }
    
    .main-calculator:hover {
      box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);
    }
    .overlay {
      background: -webkit-linear-gradient(#00466f, #e73934);
      background: linear-gradient(#00466f, #e73934);
      opacity: 0.85;
      filter: alpha(opacity=85);
      height: 460px;
      position: absolute;
      width: 360px;
      z-index: 1;
    }
    
    .close {
      position: absolute;
      right: 32px;
      top: 32px;
      width: 32px;
      height: 32px;
      opacity: 0.3;
    }
    .close:hover {
      opacity: 1;
    }
    .close:before, .close:after {
      position: absolute;
      left: 15px;
      content: ' ';
      height: 33px;
      width: 2px;
      background-color: #333;
    }
    .close:before {
      transform: rotate(45deg);
    }
    .close:after {
      transform: rotate(-45deg);
    }
      
    @media (max-width: 500px) {
      .main-calculator {
        width: 100%;
        height: 100%;
      }
      .overlay {
        width: 100%;
        height: 100%;
      }
    }
  </style>
  <button id="openbutton" class='open-button'>
    <slot name="openopenbutton-name">Open Calculator Fixed</slot>
  </button>
  <div class="container" id="modal-container">
  <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
  <a href="#" id="close-button" class="close"></a>
    <div class='main-calculator'>
      <div class='overlay'></div>
      <div class='content'>
        <div class='title'>Share Price Calculator</div>
        <div class='subtitle'>Determine the amount of FaceBook shares you can buy.</div>
        <div id="results" class='results-show'></div>
        <div class='input-parent'>
          <input type='number' id="amount" placeholder='Amount in USD' class='input-line full-width amount'></input>
        </div>
      <div class='subtitle error'>Input Error: check amount!</div>
        <div class="button-parent">
          <button id="calc-button" class='main-button full-width'>
            Calculate            
          </button>
      </div>
      </div>
    </div>
  </div>
`;

const loadingIcon = '<svg width="30px"  height="30px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-ripple" style="background: none;"><circle cx="50" cy="50" r="34.5777" fill="none" ng-attr-stroke="{{config.c1}}" ng-attr-stroke-width="{{config.width}}" stroke="#ffffff" stroke-width="2"><animate attributeName="r" calcMode="spline" values="0;40" keyTimes="0;1" dur="1" keySplines="0 0.2 0.8 1" begin="-0.5s" repeatCount="indefinite"></animate><animate attributeName="opacity" calcMode="spline" values="1;0" keyTimes="0;1" dur="1" keySplines="0.2 0 0.8 1" begin="-0.5s" repeatCount="indefinite"></animate></circle><circle cx="50" cy="50" r="15.5485" fill="none" ng-attr-stroke="{{config.c2}}" ng-attr-stroke-width="{{config.width}}" stroke="#ffffff" stroke-width="2"><animate attributeName="r" calcMode="spline" values="0;40" keyTimes="0;1" dur="1" keySplines="0 0.2 0.8 1" begin="0s" repeatCount="indefinite"></animate><animate attributeName="opacity" calcMode="spline" values="1;0" keyTimes="0;1" dur="1" keySplines="0.2 0 0.8 1" begin="0s" repeatCount="indefinite"></animate></circle></svg>'

class ShareCal extends HTMLElement {

  constructor() {
    super();
    this.root = this.attachShadow({ mode: 'open' });
    this.root.appendChild(template.content.cloneNode(true));

    /**
     * Get Elements from Component DOM.
     */
    this.amountElement = this.root.getElementById('amount');
    this.calcButton = this.root.getElementById('calc-button');
    this.modalClose = this.root.getElementById('close-button');
    this.modalContainer = this.root.getElementById('modal-container');
    this.resultsContainer = this.root.getElementById('results');
    this.modalOpen = this.root.getElementById('openbutton');

    /**
     * Add Event listeners.
     */
    this.modalClose
      .addEventListener('click', (e) => {
        this.modalContainer.style.top = "120vh";
    });
    this.modalOpen
      .addEventListener('click', (e) => {
        this.modalContainer.style.top = "0";
    });
    this.calcButton
      .addEventListener('click', (e) => {
        this.calcButton.innerHTML = loadingIcon;
        console.log(this.amountElement.value);
        this.httpGetAsync(this.amountElement.value, this.resultsContainer, this.calcButton)
    });

  }

  /**
   * Create http request to live stock API.
   */
  httpGetAsync(amount, resultsEl ,buttonEl) {
      const xmlHttp = new XMLHttpRequest();
      xmlHttp.onreadystatechange = function() {
          if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
              buttonEl.innerHTML = "Calculate";
              let pricePerShare = JSON.parse(xmlHttp.responseText).dataset.data[0][4];
              resultsEl.innerHTML = `${(amount/pricePerShare).toFixed(4)} Shares \@`;
              resultsEl.style.display = "block";
              console.log( (amount/pricePerShare).toFixed(4) );
          }
      }
      xmlHttp.open("GET", "https://www.quandl.com/api/v3/datasets/WIKI/FB.json?api_key=zTuzmFgPBtar8ek-Bdx4", true);
      xmlHttp.setRequestHeader('Access-Control-Allow-Origin', '*');
      xmlHttp.setRequestHeader('Access-Control-Allow-Methods', 'GET, POST');
      xmlHttp.setRequestHeader('Access-Control-Allow-Headers', 'Origin, Content-Type, X-Auth-Token');
      xmlHttp.send(null);
  }
}

customElements.define('capitec-share-calculator', ShareCal);